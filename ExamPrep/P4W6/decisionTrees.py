import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
import os 
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'
import graphviz
import six
import sys
sys.modules['sklearn.externals.six'] = six
from id3 import Id3Estimator, export_graphviz, export_text

stud = pd.read_csv("Questionnaire 20-21.csv", delimiter=';', decimal=',')

# What is the entropy of the table/dataframe stud?
# Which column would you use first to do a breakdown when we want to predict the mascot of a new student?

# use the ID3-functions –see P4W3 slides Decision Trees

def entropy(column: pd.Series, base=None):
    # Determine the fractions for all column values v
    fracties = column.value_counts(normalize=True, sort=False)
    base = 2 if base is None else base
    return -(fracties * np.log(fracties) / np.log(base)).sum()


def information_gain(df: pd.DataFrame, s: str, target: str):
    # calculate entropy of parent table
    entropy_parent = entropy(df[target])
    child_entropies = []
    child_weights = []
    # compute entropies of child tables
    for (label, p) in df[s].value_counts().items():
        child_df = df[df[s] == label]
        child_entropies.append(entropy(child_df[target]))
        child_weights.append(int(p))
    # calculate the difference between parent entropy and weighted child entropies
    return entropy_parent - np.average(child_entropies, weights=child_weights)


entropy(stud)
for label in stud.drop(['Class Mascot'], axis=1).columns:
    print('{}: {}'.format(label, information_gain(stud, label, 'Class Mascot')))

# Use ID3Estimator tocreate a decision tree to predict the mascot of a new student.

model = Id3Estimator()
# X = features, y = target
x = stud.drop(['Class Mascot'], axis=1).values.tolist()
y = stud['Class Mascot'].values.tolist()
model.fit(x, y)
print(export_text(model.tree_, feature_names=stud.drop(
    ['Class Mascot'], axis=1).columns))
