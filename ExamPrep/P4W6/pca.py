import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

studenq = pd.read_csv("Questionnaire 20-21.csv", delimiter=';', decimal=',')
stud_numbers = studenq[['Shoe Size', 'Length', 'Siblings',
                        'Mobile Devices', 'Travel Distance', 'Biden']].copy()
stud_numbers = stud_numbers.dropna(axis='rows')

# Check the correlations between the columns of the dataframe stud_numbers
stud_numbers.corr()

# Standardize thedata of thedataframe stud_numbers
stud_numbers_Z=pd.DataFrame()
for column in stud_numbers:
    mean = stud_numbers[column].mean()
    standev =  stud_numbers[column].std()
    stud_numbers_Z[column] = (stud_numbers[column] -mean)/standev

stud_numbers_Z.index=stud_numbers.index.values
# OR
studnumbZ = pd.DataFrame(StandardScaler().fit_transform(stud_numbers))

# Perform a PCA on the standardize data of the dataframe stud_numbers.What is the variance explained by the principal components? Show it in a graph

pca_dim = min(studnumbZ.shape[1], studnumbZ.shape[0]) 
pcamodel = PCA(n_components=pca_dim) 
principalComponents = pcamodel.fit_transform(studnumbZ)
print('Explained variance in %:', pcamodel.explained_variance_ratio_) 
labels_bar = ['PC{}'.format(i) for i in range(1, pca_dim+1)] 
plt.figure() 
plt.bar(labels_bar, pcamodel.explained_variance_ratio_) 
plt.title('PCA: Variance ratio') 
plt.xlabel('PCi') 
plt.ylabel('%') 
plt.show()
