import pandas as pd
import numpy as np

# import csv
# fields = []
# rows = []
# with open('Questionnaire 20-21.csv', newline='') as csvfile:
#     data = csv.reader(csvfile, delimiter=';')
#     fields = next(data)
#     dfB = pd.DataFrame(columns=[fields[0], fields[1]])
#     for rows in data: 
#         if len(rows) > 2: 
#             newRow = {fields[0]: rows[0], fields[1]: rows[1]+','+rows[2]}
#             dfB = dfB.append(newRow, ignore_index=True)
#         else: 
#             newRow = {fields[0]: rows[0], fields[1]: rows[1]}
#             dfB = dfB.append(newRow, ignore_index=True)


studenq = pd.read_csv("Questionnaire 20-21.csv", delimiter=';', decimal=',')

# Make Importance AI Study and study load ordered Pandas Categorical
# variables.

studenq['Importance AI Study'] = studenq['Importance AI Study'].astype(pd.CategoricalDtype(categories=['Not at all', 'Little importance','Moderate importance', 'Great importance', 'Very great importance', 'Extreemly important'], ordered = True))
studenq['Study Load']=studenq['Study Load'].astype(pd.CategoricalDtype(categories=[' <= 10 hours', '11-15 hours', '16 - 20 hours', '21-25 hours', '26-30 hours', '36-40 hours', ' >= 41 hours'], ordered=True))

# In the Blood Type and Resus Factor columns, replace the value
# 'Unknown' with NA.

studenq['Blood Type'].replace(['Unknown'], value=np.nan, inplace=True)
studenq['Resus Factor'].replace(['Unknown'], value=np.nan, inplace=True)

# Delete all rows for which there are NA values

studenq = studenq.dropna(axis='rows')

# Delete all rows for which the column Siblings contains a value smaller
# than 1

studenq = studenq[studenq.Siblings >= 1]

# Make a new dataframe studby selecting the following colomns from studenq: Writing Hand, Importance AI, Study Load, Siblings, Travel Distance, Blood Type, Driver License. 
stud = studenq[['Writing Hand', 'Importance AI Study', 'Study Load', 'Siblings', 'Travel Distance', 'Blood Type', 'Driver License']].copy()

# In dataframe stud, convert column of numeric data to categories: ▪Siblings: use the categories: 
# * "none", "one", "two", "three", "four or more" instead of a number
# * Travel Distance: use the categories: "walking distance" (2 km or less), "cycling 
# distance" (more than 2, up to and including 10 km), "bus distance" (more than 10, 
# up to and including 15 km), "train distance" (more than 15 km) 

cutpoints = [0, 1, 2, 3, 4, stud.Siblings.max()+1]
levels = ["none", "one", "two", "three", "four or more"]
stud.Siblings = pd.cut(stud.Siblings, bins=cutpoints, right=False, ordered=True, labels=levels)
cutpoints = [0, 2, 10, 15, stud['Travel Distance'].max()+1]
levels = ["walking distance", "cycling distance", "bus distance", "train distance"]
stud['Travel Distance'] = pd.cut(stud['Travel Distance'], bins=cutpoints, right=True, ordered=True, labels=levels)

# Add a column to studcontaining, for each student, the mascot animal that ranks 1st for this student.
mascot = [] 
for i in range(0, len(studenq['Class Mascot'])):
    mascot += [studenq['Class Mascot'][i].split(',')[0].split('=')[1]]
stud['Mascot'] = mascot
