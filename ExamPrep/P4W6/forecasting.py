import pandas as pd
# Generate a time series(50 time points) consisting of a(linear) trend
# (1.05 * t + 35), a seasonal fluctuation(m=4: -2, 2, 7, 3) and noise
# (values between - 5 and 5).

import random
t_a = range(0, 50, 1)
season_flux = [-2, 2, 7, 3]
period = 4
t_s = [] 
for i in t_a: 
    t_s = t_s + [1.05 * i + 35.0 + season_flux[i % period] + (random.random() * 10.0 - 5.0)]


# Apply the simple forecast models and determine which forecast best 
# approximates the time series(i.e. determine MAE, RMSE, MAPE. Use 
# m=4 where necessary).

# reliabilityTable(t_s, 4)

# Perform a seasonal decomposition(try the two models)

# t = ts(tijdreeks, frequency=4)
import statsmodels.tsa.seasonal as smts
import matplotlib.pyplot as plt

result = smts.seasonal_decompose(t_s, model='additive', period=4) 
plt.figure() 
result.plot() 
plt.show() 
result = smts.seasonal_decompose(t_s, model='multiplicative', period=4)

