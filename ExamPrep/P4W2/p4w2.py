#Exercise 1
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
import math
import numpy as np
import statsmodels.tsa.seasonal as smts

data = pd.read_csv("popularityApp.csv")

revenues = [20,100,175,13,37,136,245,26,75,155,326,48,92,202,384,82,176,282,445,181]

#a. Make a plot of the data so that you can already get some insight in the data.
def plotData(data):
	plt.figure()
	plt.plot(data['date'], data['downloads'])
	plt.xlabel('date')
	plt.ylabel('amount')
	plt.title('Amount of downloads')
	plt.show()

#plotData(data)

#b. Can you find the season size?
def determineSeasonSize(data):
	plt.figure()
	plt.acorr(data['downloads'].astype(float))
	plt.xlabel('offset')
	plt.ylabel('correlation')
	plt.title('Auto-correlation revenues')
	plt.show()

#better
def determineSeasonSizeStatsmodel(data):
	plt.figure()
	plot_acf(data['downloads'].astype(float), lags=15)
	plt.xlabel('offset')
	plt.ylabel('correlation')
	plt.title('Auto-correlation revenues')
	plt.show()

#determineSeasonSize(data)
#determineSeasonSizeStatsmodel(data)

#c. Forecast now, for each prediction method, the next three days. Calculate the MAE, RMSE and
#MAPE each time. Complete the following table with all results:
def calculatePreviousForecasting(past, predictor):
	predicted = []
	n = len(past)
	for i in range(0,n):
		predicted = predicted + [predictor(past[0:i])]
	return predicted


def naiveForecasting(past):
	if (len(past)<1):
		return math.nan
	return past[len(past)-1]

def averageForecasting(past):
	if (len(past)<1):
		return math.nan
	return pd.Series(past).mean()

def movingAverageForecasting(period):
	def result(past):
		n = len(past)
		if (n < period):
			return math.nan
		return pd.Series(past[(n-period):n]).mean()
	return result

def calculateWeights(period, past):
    n = len(past)
    if (n<2*period):
        return math.nan
    v = past[(n-2*period):(n-period)]
    for i in range(2,period+1):
        v = v + past[(n-2*period+i-1):(n-period+i-1)]
    M = np.array(v).reshape(period, period)
    v = past[(n-period):n]
    return np.linalg.solve(M, v)

def linearCombinationForecasting(period):
	#for linear combination: data has to be a list (not numpy array, df column,..) -> ex. print(linearCombinationForecasting(13)(list(data['downloads']))) works
    def result(past):
        n = len(past)
        if (n<2*period):
            return math.nan
        a = calculateWeights(period, past)
        return (past[(n-period):n]*a).sum()
    return result

def general_regression(x, y, degree=1, exp=False):
    func=lambda x:x # def fun(x): return[x]
    inv_func=lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x:func(line(x))
    yyy = pd.Series(predict(x))
    se = math.sqrt(((yyy-y)**2).mean())
    R2 = (x.corr(inv_func(y)))**2
    result = [se, R2, predict]
    index = ['se', 'R2', 'predict']
    for i in range(1,len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

def trendForecastingModel(past):
	n = len(past)
	x = pd.Series(range(0,n))
	y = pd.Series(past)
	reg = general_regression(x, y)
	return reg.predict

def additiveDecompositionModel(past):
	past = smts.seasonal_decompose(past, model='additive', period=7)
	return past

def multiplicativeDecompositionModel(past):
	past = smts.seasonal_decompose(past, model='multiplicative', period=7)
	return past

def calculateMAE(data):
	return data.abs().mean()

def calculateRMSE(data):
	return math.sqrt((data**2).mean())

def calculateMAPE(data, past):
	return (data/past).abs().mean()

def calculateMultipleDays(data, amountOfDays):
	for i in range(0, amountOfDays):
		#change function to whatever is needed
		processedData = linearCombinationForecasting(13)(list(data))
		print(processedData)
		data = data.append(pd.Series(processedData))

#specific function for trend forecasting
def calculateMultipleDaysTrendForecasting(data, amountOfDays):
	for i in range(0, amountOfDays):
		processedData = trendForecastingModel(data)
		dataLength = len(data)
		print(processedData(dataLength))
		data = data.append(pd.Series(processedData(dataLength)))

def calculateSeason(period):
	SandR = pd.Series(data['downloads']) - trend
	n = len(SandR)
	seasonal = []
	for i in range(0, period):
		seasonal = seasonal + [SandR[list(range(i, n, period))].mean()]
	print(seasonal )

def findTrend(x, period):
	result = smooth(x, period)
	nan = [math.nan] * int(period/2)
	if (period % 2 == 0):
		result = smooth(result, 2)
	result = nan + result + nan
	return result

def smooth(x, period):
	result = []
	for i in range(0, len(x)-period+1):
		result = result + [np.mean(x[i:i+period])]
	return result

#Heads up: change to list(data['downloads']) when using linearCombinationForecasting, when using multiple

#predicted = calculatePreviousForecasting(data['downloads'], naiveForecasting)
#predicted = calculatePreviousForecasting(data['downloads'], averageForecasting)

#can replace the second parameter in calculatePreviousForecasting with forecast if needed (both work the same)
#Working for movingAverageForecasting and linear
#forecast = movingAverageForecasting(5)
#predicted = calculatePreviousForecasting(data['downloads'], forecast)
#errors = pd.Series(predicted) - data['downloads']

#calculating errors with trend for MAE, RMSE and MAPE:
myTrend = trendForecastingModel(data['downloads'])

#31 = the amount of data (can use len() because amount varies from dataframe to dataframe, 31 is the amount in this scenario)
#predicted = pd.Series(myTrend(range(0,31)))

predicted = pd.Series(myTrend(range(0,len(data))))

errors = predicted - data['downloads']

#other method
#forecast = movingAverageForecasting(5)
#forecast(data['downloads'])

#print(movingAverageForecasting(5)(data['downloads']))
#print(linearCombinationForecasting(13)(list(data['downloads'])))

#calculateMultipleDaysTrendForecasting(data['downloads'], 3)

#print(calculateMultipleDays(data['downloads'], 3))

#Calculate MAE, RMSE and MAPE

print(calculateMAE(errors))
print(calculateRMSE(errors))
print(calculateMAPE(errors, data['downloads']))

#Prediction method, day 31, day 32, day 33, MAE, 		RMSE, 		MAPE
#Naïve: 			34, 	34, 	34, 	1.533333, 	1.914854, 	0.1199493
#Average:			17.4387,17.48387,14.48387,8.2082828,9.302767,	0.4338687
#Moving (m=5)		30.6,	31.120,	31.344,	3.130769,	3.366578,	0.1835417
#Linear comb(m=13)	36.8321,37.3841,42.0988,1.531631,	1.780848,	0.0488565
#Trend 				33.30968,34.29879,35.28790,1.0036420,1.156301,	0.104034
#additive decomp.	
#multip. decomp.	

#d. Linear combination: Which weights do you find? Which value plays the greatest role in predicting
#the next value?

#print(calculateWeights(13,list(data['downloads'])))

additiveDecomp = additiveDecompositionModel(data['downloads'])
#print(additiveDecomp.trend)

#e. Trend estimation: What is the formula of the regression line? e. 2.64718 + 0.9891129 * index 
myTrend = trendForecastingModel(data['downloads'])
myTrend = trendForecastingModel(revenues)
print(myTrend(0))

#additive: S(i) + R(i) = xi - T(i)
#mutliplicative: S(i) . R(i) = xisqrt(T(i))


#f. Additive decomposition: What is the formula for the trend line? 2.82659 + 0.973846 * index (small difference with trend estimation due to the use of less datapoints)

#g. Additive decomposition: What are the values for the recurring pattern (seasonal trend)? 
period = 7
trend = findTrend(data['downloads'], period)
SandR = pd.Series(data['downloads']) - trend
seasonFactor = []
for i in range(0, period):
	seasonValues = SandR[list(range(i, len(SandR), period))]
	seasonValues = seasonValues[~(np.isnan(seasonValues))]
	seasonFactor = seasonFactor + [seasonValues.mean()]

print(seasonFactor)

seasonal = []

for i in range(0, len(data['downloads'])):
	seasonal = seasonal+[seasonFactor[i%period]]
noise = pd.Series(data['downloads']) - trend - seasonal
print("seasonal: " + str(seasonal))

print("errors: ")
print(calculateMAE(noise))
print(calculateRMSE(noise))
print(calculateMAPE(noise, data['downloads']))

#h. Multiplier decomposition: What is the formula for the trend line? 2.64718 + 0.9891129 * index (the same as with the additive)
low = period//2
high = len(revenues)-period//2

# determine the best fitting regression

# regression - cubic

reg = general_regression(pd.Series(
range(low, high)), pd.Series(trend[low: high]), 3)
# Forecast

tx = 25
predicted = reg.predict(range(tx, tx+1)) + seasonFactor[tx%period]

#i. Multiplicative decomposition: What are the values for the recurring pattern (seasonal trend)?

#j. Which technique gives the best prediction and why? --> linear, in this situation this has the lowest MAPE value (the lower, the more accurate)
