import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

cpus = pd.read_csv("cpus.csv", delimiter=";")

#a. Which  columns  do  you  need  to  remove in order  to  perform  a  Principal  Component  Analysis?  Remove any columns with the wrong scale, columns that do not make sense, ....
cpusCorrect = cpus.drop(['name'], axis=1)
print(cpusCorrect)

#b. Look  at  the  correlations  between  the  variables.  Is  the  data  usable for Principal  Component Analysis?
#Yes, there are a lot of high correlations
print(cpusCorrect.corr(method="pearson"))

#c. Perform a Principal Component Analysis and interpret the results
#First 4 principal components are sufficient for 90%
#PC8 is less than 1%
cpusCorrectZ =pd.DataFrame()
for column in cpusCorrect:
	mean = cpusCorrect[column].mean()
	standev = cpusCorrect[column].std()
	cpusCorrectZ[column] = (cpusCorrect[column] - mean)/standev
cpusCorrectZ.index= cpusCorrect.index.values
pca_dim = min(cpusCorrectZ.shape[1], cpusCorrectZ.shape[0])
pcamodel = PCA(pca_dim)
principalComponents = pcamodel.fit_transform(cpusCorrectZ)
print('Explained variance in %:', pcamodel.explained_variance_ratio_)

#d. Are there one or more variables that weigh heavily in determining the first Principal Component? If so, which ones?
#No, all variables play a role
row_labels = ['PC{}'.format(i) for i in range(1,pca_dim+1)]
aij=pd.DataFrame(data=pcamodel.components_,	columns = cpusCorrect.columns, index = row_labels)

print(aij)

#e. Visualise theexplainedvariance and make abiplot(PC1 and PC2
def putIntoBarChart():
	labels_bar = ['PC{}'.format(i) for i in range(1,pca_dim+1)]
	plt.figure()
	plt.bar(labels_bar, pcamodel.explained_variance_ratio_)

	plt.title('PCA: Variance ratio')
	plt.xlabel('PCi')
	plt.ylabel('%')
	plt.show()

def showBiPlot():
	putIntoBarChart()
	principalDf= pd.DataFrame()
	principalDf['PC1'] = principalComponents[:,0]
	principalDf['PC2'] = principalComponents[:,1]
	fig, ax = plt.subplots(figsize=(5, 5))
	ax.set_xlabel('Principal Component 1')
	ax.set_ylabel('Principal Component 2')
	ax.set_title('2-dim PCA')
	markers = list(matplotlib.markers.MarkerStyle.markers.keys())
	for m in zip(markers):
		ax.scatter(principalDf['PC1'], principalDf['PC2'], s = 50)
	ax.grid(True)
	plt.show()

showBiPlot()

#f. Based on the graph with theexplainedvariance, how many main components would you hold back when 'reducing' the dataset?
#First three, they're the highest, rest are less than 10%