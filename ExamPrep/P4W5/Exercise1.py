import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn.neighbors import DistanceMetric
from sklearn.cluster import AgglomerativeClustering

#a. Place the data in a data frame.
proteinConsumption = pd.read_csv('Protein consumption in 25 European countries.csv', delimiter=';', decimal=',')
proteinConsumption = proteinConsumption.dropna()

#c. Which columns do you need to remove in order to perform a Principal Component Analysis?
#Remove any columns with the wrong scale, columns that do not make sense,.....
#Country and Total --> wrong scale (and don't make any sense)
proteinConsumptionCorrect = proteinConsumption.drop(['Country','Total'], axis=1)
print(proteinConsumptionCorrect)

#d. Look at the correlations between the variables. Is the data usable for Principal Component
#Analysis? --> proteinConsumption.corr(method="kendall")
#Yes, is usable.
proteinConsumptionCorrelation = proteinConsumption.corr(method="pearson")
#print(proteinConsumptionCorrelation)

#e. Perform a Principal Component Analysis -do not forget to normalise the data- and interpret
#the results.
#proteinConsumptionCorrectZ = StandardScaler().fit_transform(proteinConsumptionCorrect)
proteinConsumptionCorrectZ =pd.DataFrame()
for column in proteinConsumptionCorrect:
	mean = proteinConsumptionCorrect[column].mean()
	standev = proteinConsumptionCorrect[column].std()
	proteinConsumptionCorrectZ[column] = (proteinConsumptionCorrect[column] - mean)/standev
proteinConsumptionCorrectZ.index= proteinConsumptionCorrect.index.values
pca_dim = min(proteinConsumptionCorrectZ.shape[1], proteinConsumptionCorrectZ.shape[0])
pcamodel = PCA(pca_dim)
principalComponents = pcamodel.fit_transform(proteinConsumptionCorrectZ)
print('Explained variance in %:', pcamodel.explained_variance_ratio_)

def putIntoBarChart():
	labels_bar = ['PC{}'.format(i) for i in range(1,pca_dim+1)]
	plt.figure()
	plt.bar(labels_bar, pcamodel.explained_variance_ratio_)

	plt.title('PCA: Variance ratio')
	plt.xlabel('PCi')
	plt.ylabel('%')
	plt.show()

#putIntoBarChart()

#f. Are there one or more variables that weigh heavily in determining the first Principal
#Component? If so, which ones?
row_labels = ['PC{}'.format(i) for i in range(1,pca_dim+1)]
aij=pd.DataFrame(data=pcamodel.components_,	columns = proteinConsumptionCorrect.columns, index = row_labels)

print(aij)

#g. Make a biplot (PC1 and PC2)
def showBiPlot():
	principalDf= pd.DataFrame()
	principalDf['PC1'] = principalComponents[:,0]
	principalDf['PC2'] = principalComponents[:,1]
	fig, ax = plt.subplots(figsize=(5, 5))
	ax.set_xlabel('Principal Component 1')
	ax.set_ylabel('Principal Component 2')
	ax.set_title('2-dim PCA')
	markers = list(matplotlib.markers.MarkerStyle.markers.keys())
	for m in zip(markers):
		ax.scatter(principalDf['PC1'], principalDf['PC2'], s = 50)
	ax.grid(True)
	plt.show()

#showBiPlot()

#h. Create a new data frame, taking the first three main components for the observations.
col_names = ['PC{}'.format(i) for i in range(1,4)]
newProteinConsumptionData = pd.DataFrame(data=principalComponents[:,[0,1,2]], columns= col_names)
#print(newProteinConsumptionData)

#i. Apply a hierarchical cluster analysis to this (Euclidean distance). Compare the results with the
#results obtained with a cluster analysis applied to the original variables. Compare also with the
#biplot.

dist_obj = DistanceMetric.get_metric('euclidean')

def drawDendogram(data):
	new_Data_Frame = pd.DataFrame(data=principalComponents[:,0:3], index=proteinConsumption.Country.index, columns=['PC1', 'PC2','PC3'])

	new_Data_hierarchical_clusters = new_Data_Frame.copy()
	new_Data_hierarchical_clusters.index = range(1, new_Data_hierarchical_clusters.shape[0]+1)
	hcmodel = AgglomerativeClustering(n_clusters=4)
	hcmodel.fit(new_Data_hierarchical_clusters)

	colors = list(matplotlib.colors.cnames.keys())

	fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(5,5))
	distances = linkage(new_Data_hierarchical_clusters, method='single')
	ax.set_title('Dendrogram')
	ax.set_xlabel('point')
	ax.set_ylabel('Euclidean distance')
	ax.grid(linestyle='--', axis='y')
	dgram=dendrogram(distances, labels=list(range(1,new_Data_hierarchical_clusters.shape[0]+1)), link_color_func=lambda x: colors[x], leaf_font_size=15, ax=ax)
	plt.show()

drawDendogram(dist_obj.pairwise(proteinConsumptionCorrect))