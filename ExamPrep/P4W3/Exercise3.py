# Decision Trees
import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
import os 
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'
import graphviz
import six
import sys
sys.modules['sklearn.externals.six'] = six
from id3 import Id3Estimator, export_graphviz, export_text

def entropy(column: pd.Series, base=None):  
    # Determine the fractions for all column values v
    fracties = column.value_counts(normalize=True, sort=False)
    base = 2 if base is None else base
    return -(fracties * np.log(fracties) / np.log(base)).sum()

def information_gain(df: pd.DataFrame, s: str, target: str):
    # calculate entropy of parent tabl
    entropy_parent = entropy(df[target])
    child_entropies = []
    child_weights = []
    # compute entropies of child tables
    for (label, p) in df[s].value_counts().items():
        child_df = df[df[s] == label]
        child_entropies.append(entropy(child_df[target]))
        child_weights.append(int(p))
        # calculate the difference between parent entropy and weighted child entropies
        return entropy_parent -np.average(child_entropies, weights=child_weights)

#a. Load the file "studentsScores.csv". Here you can see the subjective scores, given by the
#teacher. The score can be: "good", "average", "bad". We are now wondering on the basis
#of which criteria the teacher has given his scores.
studentScores = pd.read_csv('studentsScores.csv')

#b. To do this, set up a decision tree for the score with ID3Estimator.

# def ID3Tree():
# 	model = Id3Estimator()
# 	# X = features, y = target
# 	X = studentScores.drop(['score'], axis=1).values.tolist()
# 	Y = studentScores['score'].values.tolist()
# 	model.fit(X,Y)
# 	print(export_text(model.tree_, feature_names=studentScores.drop(['score'], axis=1).columns))

# 	# Export decision tree to disk
# 	export_graphviz(model.tree_,'tree.dot', feature_names=studentScores.drop(['score'], axis=1).columns)

# 	dot = graphviz.Source.from_file('tree.dot')
# 	dot.render('tree', view=True)

#ID3Tree()
#c. Which subjects does the teacher teach? --> Looking from the ID3Tree, only subject 1 and 4 are used --> teacher's subjects


#d. We dividing the points into categories: not successful (0-9), satisfactory (10-13), honors
#(14-15), highest honors (16-20). Try to classify the scores as mentioned.
def addCategories(data):
    studentsScores_cat = pd.DataFrame()
    for label in data.drop(['score'], axis=1).columns:
        categor = []
        for i in range(0, data.shape[0]):
            if data.loc[i, label] < 10:
                categor = categor + ['not successful (0-9)']
            else:
                if data.loc[i, label] < 14:
                    categor = categor + ['satisfactory (10-13)']
                else:
                    if data.loc[i, label] < 16:
                        categor = categor + ['honors (14-15)']
                    else:
                        categor = categor + ['highest honors (16-20)']
        studentsScores_cat[label] = categor
    studentsScores_cat['score'] = data.score
    return studentsScores_cat

studentScoresCategories = addCategories(studentScores)
print(studentScores)
print(studentScoresCategories)

#e. Now recalculate the tree structure. Is this the same?

def ID3Tree():
	model = Id3Estimator()
	# X = features, y = targets
	X = studentScores.drop(['score'], axis=1).values.tolist()
	Y = studentScoresCategories['score'].values.tolist()
	model.fit(X, Y)
	print(export_text(model.tree_, feature_names=studentScoresCategories.drop(
	    ['score'], axis=1).columns))

	# Export decision tree to disk
	export_graphviz(model.tree_, 'tree.dot',
	                feature_names=studentScoresCategories.drop(['score'], axis=1).columns)

	dot = graphviz.Source.from_file('tree.dot')
	dot.render('tree', view=True)

ID3Tree()

#f. How would the teacher judge someone if they have the following points:
#9, 14, 13, 12, 15, 13, 7, 10, 18

def testNewStudent():
    model = Id3Estimator()
    # X = features, y = target
    X = studentScores.drop(['score'], axis=1).values.tolist()
    Y = studentScores['score'].values.tolist()
    model.fit(X,Y)
    newStudent = np.array([[9, 14, 13, 12, 15, 13, 7, 10, 18]])
    print(model.predict(newStudent))

#new student = "bad" grade
#testNewStudent()

#g. Use the tree structure with the original data (see b.).
#Do you get the same result for the student above? --> yes, when looking at the tree, the subject has a 12 in subject 4 and a 9 in subject 1. This leads to a "bad" score
