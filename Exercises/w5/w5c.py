import pandas as pd
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

lengthMartians = open('inhabitantsonmars.txt', 'r')
arrayLengthMartians = lengthMartians.read().split()
dfLengthMartians = {'length' : []}
dfLengthMartians = pd.DataFrame(dfLengthMartians, columns=['length'])
dfLengthMartians['length'] = arrayLengthMartians
dfLengthMartians['length'] = dfLengthMartians['length'].astype(float)

#(a) What is the mean length of a Martian?
print(pd.to_numeric(dfLengthMartians['length'], downcast="float").dropna().mean())
#(b) What is the median? What does it mean?
print(pd.to_numeric(dfLengthMartians['length'], downcast="float").dropna().median())
#(c) What is the average absolute deviation? What does this mean?
print(pd.to_numeric(dfLengthMartians['length'], downcast="float").dropna().mad())
#(d) What is the standard deviation? What does this mean?
print(pd.to_numeric(dfLengthMartians['length'], downcast="float").dropna().std())
#(e) Determine the quartiles and the deciles.
#Can remove all pd.to_numeric calls, problem was that the column was object and I didn't realize
def printQuartiles(data):
	return "Q1: " + str(pd.to_numeric(dfLengthMartians['length']).quantile(0.25)) + " Q2: " + str(pd.to_numeric(dfLengthMartians['length']).quantile(0.50)) + " Q3: " + str(pd.to_numeric(dfLengthMartians['length']).quantile(0.75))

def printDeciles(data):
	decileAmount = 0.10
	stringDeciles = ''
	while decileAmount <= 0.90:
		stringDeciles += "Decile " + str(round(decileAmount, 1)) + ": " + str(round(pd.to_numeric(data).quantile(decileAmount), 6)) + "\n"
		decileAmount += 0.10
	return stringDeciles

print(printQuartiles(dfLengthMartians['length']))
print(printDeciles(dfLengthMartians['length']))

#(g) The percentage of Martians that is smaller than or equal to 60 cm is ... (experiment with the quantiles to find this)
print(100*(dfLengthMartians['length'].loc[pd.to_numeric(dfLengthMartians['length']) <= 60].count() / dfLengthMartians['length'].count()))

#(h) Draw a box plot. What information do you see here?
#Draw a box plot
dfLengthMartians['length'] = dfLengthMartians['length'].astype(float)
def showBoxPlot(data):
	plt.figure()
	#change to what you need
	dataToPlot = 'length'
	data.dropna().boxplot(dataToPlot)
	plt.show()


#(i) Make classes of 5 cm and determine the frequencies (let the classes start at 0 cm). What is the mode?
cutpoints = range(0, 120, 5)
classes = pd.cut(dfLengthMartians['length'], bins=cutpoints, right=False)

#Print classes (print(classes.value_counts().sort_index())) and look at the most popular and take that range --> answer: mode = class 40,45 with class representative 42.5 cm
print(classes.value_counts().sort_index())

#(j) Make classes of 5 cm and draw a histogram. What do you see? How would you explain this? Do you see this when you make classes of 10 cm?
heightMartiansDataFrame = {'bins' : [],
				 'values' : []}

heightMartiansDataFrame = pd.DataFrame(heightMartiansDataFrame,columns=['bins','values'])

heightMartiansDataFrame['bins'] = cutpoints
heightMartiansDataFrame['values'] = classes.value_counts().sort_index()
heightMartiansDataFrame.columns = ['bins','values']

def histogramQ8():
	#from 0 to 10
	#change frequency to needed row
	#also works: frequency=[0,1,3,2,3,5,6,3,2,0]
	"""
	example, also works with arrays	"""
	dfq8 = {'score': ['1','2','3','4','5','6','7','8','9','10'],
		'frequency': [0,1,3,2,3,5,6,3,2,0]}
	dfq8 = pd.DataFrame(dfq8, columns=['score','frequency'])

	score = range(1,11,1)
	plt.figure()
	ax = plt.axes()
	ax.set_xticks(heightMartiansDataFrame['bins'])
	plt.bar(heightMartiansDataFrame['bins'], heightMartiansDataFrame['values'])
	plt.show()

#Now open the files "males.txt" and "females.txt". These files contain the lengths of the same Martians, but now by gender.
lengthMaleMartians = open('males.txt', 'r')
arrayLengthMaleMartians = lengthMaleMartians.read().split()
dfLengthMaleMartians = {'length' : []}
dfLengthMaleMartians = pd.DataFrame(dfLengthMaleMartians, columns=['length'])
dfLengthMaleMartians['length'] = arrayLengthMaleMartians
dfLengthMaleMartians['length'] = dfLengthMaleMartians['length'].astype(float)

lengthFemaleMartians = open('females.txt', 'r')
arrayLengthFemaleMartians = lengthFemaleMartians.read().split()
dfLengthFemaleMartians = {'length' : []}
dfLengthFemaleMartians = pd.DataFrame(dfLengthFemaleMartians, columns=['length'])
dfLengthFemaleMartians['length'] = arrayLengthFemaleMartians
dfLengthFemaleMartians['length'] = dfLengthFemaleMartians['length'].astype(float)

#(k) What are the mean lengths of a male Martian and a female Martian?
print('Means of male and female martians seperate: ')
print(dfLengthMaleMartians.mean())
print(dfLengthFemaleMartians.mean())
#(l) Calculate the medians
print('Medians of male and female martians seperate: ')
print(dfLengthMaleMartians.median())
print(dfLengthFemaleMartians.median())
#(m) Calculate the standard deviations
print('Standard deviation of male and female martians seperate: ')
print(dfLengthMaleMartians.std())
print(dfLengthFemaleMartians.std())
#(n) Determine the quartiles
print('Quartiles of male and female martians seperate: ')

def printQuartilesMale():
	return "Q1: " + str(pd.to_numeric(dfLengthMaleMartians['length']).quantile(0.25)) + " Q2: " + str(pd.to_numeric(dfLengthMaleMartians['length']).quantile(0.50)) + " Q3: " + str(pd.to_numeric(dfLengthMaleMartians['length']).quantile(0.75))

def printQuartilesFemale():
	return "Q1: " + str(pd.to_numeric(dfLengthFemaleMartians['length']).quantile(0.25)) + " Q2: " + str(pd.to_numeric(dfLengthFemaleMartians['length']).quantile(0.50)) + " Q3: " + str(pd.to_numeric(dfLengthFemaleMartians['length']).quantile(0.75))

print(printQuartilesMale())
print(printQuartilesFemale())

#(o) Make a box plot where you see these two distributions next to each other
def showBoxPlotMaleAndFemale():
	plt.figure()
	data = [dfLengthMaleMartians['length'], dfLengthFemaleMartians['length']]
	fig = plt.figure(figsize=(10,7))
	ax = fig.add_axes([0,0,1,1])
	bp = ax.boxplot(data)
	plt.show()

#showBoxPlotMaleAndFemale()

def get_outliers(data):
	quartiles = data.quantile(q=[0.25, 0.5, 0.75])
	Q1 = quartiles.iloc[0]
	Q3 = quartiles.iloc[2]
	IQR = Q3 - Q1
	return data[(data<(Q1 - IQR * 1.5)) | (data > (Q3 + IQR * 1.5))]



def get_extreme_outliers(data):
	quartiles = data.quantile(q=[0.25, 0.5, 0.75])
	Q1 = quartiles.iloc[0]
	Q3 = quartiles.iloc[2]
	IQR = Q3 - Q1
	return data[(data<(Q1 - IQR * 3)) | (data > (Q3 + IQR * 3))]

print('Outliers Male ')
print(get_outliers(dfLengthMaleMartians['length']).mean())
print('Extreme outliers Male')
print(get_extreme_outliers(dfLengthMaleMartians['length']))

print('Outliers Female ')
print(get_outliers(dfLengthFemaleMartians['length']).mean())
print('Extreme outliers Female')
print(get_extreme_outliers(dfLengthFemaleMartians['length']))