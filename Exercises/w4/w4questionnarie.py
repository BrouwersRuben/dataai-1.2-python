import pandas as pd
import matplotlib.pyplot as plt

studenq = pd.read_csv('Questionnaire 20-21.csv', delimiter=';', decimal=',')
#How many % of students have 3 or fewer siblings?
print('How many % of students have 3 or fewer siblings?')
print(studenq.Siblings.value_counts(normalize=True).sort_index().cumsum()*100)

#How many students with blood type O have smoked in the past year?
print("How many students with blood type O have smoked in the past year?")
"""
smokerBloodTypeO = studenq.loc[(studenq['Blood Type'] == 'O') & (studenq['Smoker'] == 'Yes')]
print(str(len(smokerBloodTypeO)) + "\n")"""
#Better solution
print(pd.crosstab(studenq['Blood Type'], studenq.Smoker, margins=True))


#Create a pie chart of the number of pieces of fruit the students eat per day.
print('Create a pie chart of the number of pieces of fruit the students eat per day.')
amountFruitPerStudent = studenq['Fruit']
#Convert 1 or less to an int
l = ['1 or less', '2', '3', '4']
plt.figure()
plt.pie(amountFruitPerStudent, labels=l)
plt.title('Fruit')
plt.show()