import pandas as pd
import numpy as np
import math

studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter=';', decimal=',')

def general_regression(x, y, degree=1, exp=False, log=False):
    data = pd.DataFrame({'x': x, 'y': y})
    data.reset_index(drop=True, inplace=True)
    func = lambda x: x  # def func(x): return[x]
    inv_func = lambda x: x
    if (exp):
        func = np.exp
        inv_func = np.log
    elif (log):
        x = np.log(x)
    sy = data.y.std()
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x: func(line(x))
    data['y_pred'] = pd.Series(predict(x))
    se = math.sqrt(((data.y_pred - data.y) ** 2).mean())
    R2 = 1 - (se ** 2) / (sy ** 2)
    result = [se, R2, predict]
    index = ['se', 'R²', 'predict']
    for i in range(1, len(model) + 1):
        result = np.append(result, model[-i])
        index += chr(i + 96)  # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

import matplotlib.pyplot as plt

test = general_regression(studenq['Travel Time'], studenq['Travel Distance'])
print(test.predict(100))



plt.figure()
cutPoints = [35, 40, 45, 50, 55, 60, 65]
classes = pd.cut(marsians.stack(), bins=cutPoints)
l = ['35cm', '40cm', '45cm', '50cm', '55cm', '65cm']
x = classes.value_counts().sort_index()
plt.bar(l, x)
plt.title("Martian's Heights")
plt.xlabel('Martian Height')
plt.ylabel('Number of Martians')
plt.show()