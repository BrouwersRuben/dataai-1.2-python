import pandas as pd

studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter=';', decimal=',')

df1 = pd.DataFrame(studenq, columns = ['Languages', 'Blood Type'])

# df1['percent'] = (df1.count() / df1['Blood Type'].sum()) * 100

license = studenq[studenq['Driver License'] == 'Yes']
print(license.shape[0]/studenq.shape[0]*100, '%')

left = studenq[studenq['Writing Hand'] == 'Right']
print(left['Mobile Devices'].mean())

studenq['Nb Shoes'] = studenq['Length'] / studenq['Shoe Size']

print(studenq['Nb Shoes'].min())
print(studenq['Nb Shoes'].max())
print(studenq['Nb Shoes'].mean())

print(studenq['Siblings'].sum())

import matplotlib.pyplot as plt

studenq.plot.scatter(x='Travel Distance', y='Travel Time', title= 'Travel to KdG ')
plt.show()