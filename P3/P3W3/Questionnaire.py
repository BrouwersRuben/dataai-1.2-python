import pandas as pd
import numpy as np

studenq = pd.read_csv('csv/Questionnaire 20-21.csv' , delimiter=';', decimal=',')

# To see the content of the dataframe:
print(studenq)

# Not all columns are shown.
pd.set_option('display.max_columns', None)
print(studenq)

# Not all rows are shown.
pd.set_option('display.max_row', None)
print(studenq)

#Make Blood Type a disordered Pandas Categorical variable. The value 'Unknown' must not be converted to a missing value.
print(studenq.dtypes)
studenq['Blood Type'] = studenq['Blood Type'].astype(pd.CategoricalDtype(categories=studenq['Blood Type'].unique()))
print(studenq.dtypes)

#Make Importance AI Study an ordered Pandas Categorical variable.
studenq['Importance AI Study'] = studenq['Importace AI Study'].astype(pd.CategoricalDtype(categories=['Not at all', 'Little importance','Moderate importance', 'Great importance', 'Very great importance', 'Extreemly important'], ordered=True))

#Make also a disordered Pandas Categorical variable for the colums:
#Writing Hand, Resus Factor, Internet Purchase, Leisure, Antwerp, Address, OS, App.
studenq["Writing Hand"] = studenq["Writing Hand"].astype(pd.CategoricalDtype(categories=studenq["Writing Hand"].unique()))
studenq["Resus Factor"] = studenq["Resus Factor"].astype(pd.CategoricalDtype(categories=studenq["Resus Factor"].unique()))
studenq["Internet Purchase"] = studenq["Internet Purchase"].astype(pd.CategoricalDtype(categories=studenq["Internet Purchase"].unique()))
studenq["Leisure"] = studenq["Leisure"].astype(pd.CategoricalDtype(categories=studenq["Leisure"].unique()))
studenq["Antwerp Address"] = studenq["Antwerp Address"].astype(pd.CategoricalDtype(categories=studenq["Antwerp Address"].unique()))
studenq["OS"] = studenq["OS"].astype(pd.CategoricalDtype(categories=studenq["OS"].unique()))
studenq["App"] = studenq["App"].astype(pd.CategoricalDtype(categories=studenq["App"].unique()))

# Make a new dataframe df_selection by selecting the following
# colomns from studenq: Writing Hand, Blood Type, Importance AI
# Study , Smoker. Give the colums the names C1, C2, C3 and C4.
df_selection = studenq[['Writing Hand','Blood Type','Importance AI Study', 'Smoker']]
df_selection.columns = ['C1', 'C2', 'C3', 'C4']
df_selection = df_selection.dropna(axis='rows')

# Create a Pandas data frame that lists for each student their
# favorite fruit in descending line of preference.
#       Write the function split_choices
#       Write the function split_preferences
#       Write the function sort_preferences
#       Combine the results in a dataframe

def split_choices(row):
      return str.split(row,',')

split_choices(studenq['Favorite Fruit'][0]) # test the function on the first entry
choices_matrix = studenq['Favorite Fruit'].apply(split_choices)
choices_matrix.head()

def split_preferences(row):
      return np.array([str.split(el,'=') for el in row])
split_preferences(choices_matrix[0]) # test the function on the first entry
preferences_matrix = choices_matrix.apply(split_preferences)
preferences_matrix.head()

def sort_preferences(row):
      try:
            ranknumbers = row[:,0].astype(int) # first colomn contains the ranknumbers
            return row[ranknumbers.argsort()][:,1] # second colomn contains the types of fruit
      except Exception: # eg. when no numbers used
            return np.repeat(np.nan, 10)

sort_preferences (preferences_matrix[0]) # test the function on the first entry

preferences_t1 = '11=Kiwi,6=Pineapple,7=Cherry,8=Banana,9=Strawberry,10=Apple,1=Melon,2=Pear,3=Plum,4=Orange'
sort_preferences(split_preferences(split_choices(preferences_t1)))
preferences_t2 = 'a=Kiwi,6=Pineapple,7=Cherry,8=Banana,9=Strawberry,10=Apple,1=Melon,2=Pear,3=Plum,4=Orange'
sort_preferences(split_preferences(split_choices(preferences_t2)))
fruit_series = preferences_matrix.apply(sort_preferences) # only if no anomalies in input
fruit_series.head()

# We zip, all first choices to one tuple, all second choices to one tuple, all third choices...
# Each tuple becomes a row of the new dataframe which we then transpose.
fruit_preferences = pd.DataFrame(zip(*fruit_series), index=range(1,11)).transpose()
fruit_preferences.head()

# Create a Pandas data frame that lists for each student their
# favorite class mascot in descending line of preference
mascot_preferences = pd.DataFrame(zip(*studenq['Class Mascot'].apply(split_choices).apply(split_preferences).apply(sort_preferences)), index=range(1,11)).transpose()