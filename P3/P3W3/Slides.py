import pandas as pd

#When reading a csv, the columns are not correctly recognized.
'''
dfA = pd.read_csv('csv/errorFile3.csv', sep=',')
print(dfA)'''

dfA = pd.read_csv('csv/errorFile3.csv', sep=';')
print(dfA)

print(" "
      "")

#The number of fields in some lines do not match the number of colum names
#dfB = pd.read_csv('csv/errorFile1.csv')

import csv
fields = []
Rows = []
with open('csv/errorFile1.csv', newline='') as csvfile:
    data = csv.reader(csvfile, delimiter=',')
    fields = next(data)
    dfB = pd.DataFrame(columns=[fields[0],fields[1]])
    for rows in data:
        if len(rows) > 2:
            newRow={fields[0]:rows[0],fields[1]:rows[1]+','+rows[2]}
            dfB=dfB.append(newRow, ignore_index = True)
        else:
            newRow={fields[0]:rows[0],fields[1]:rows[1]}
            dfB=dfB.append(newRow, ignore_index = True)

print(dfB)

print(""
      " ")

# "Decimal numbers" are miscognized
dfC = pd.read_csv("csv/errorFile2.csv", sep=";")
print(dfC)
dfC.dtypes

print(" ")

dfC = pd.read_csv("csv/errorFile2.csv", sep=";", na_values = "Missing")
print(dfC)
#Transform to numerical value.
dfC[dfC.columns[1]] = dfC[dfC.columns[1]].str.replace(",", ".")
dfC[dfC.columns[1]] = pd.to_numeric(dfC[dfC.columns[1]], errors='coerce')

print(" "
      "")

#How do you determine if there are missing values (NA – Not Available) in the data?
import numpy as np
result = ['Pass', 'Fail', np.nan, 'Fail', 'Fail']
id = np.arange(1,6)
dfD= pd.DataFrame({'ID':id, 'Result':result})
dfD['Result'].isna().sum()
dfD['Result'].isna()
dfD.info()

print(""
      " ")

#How do you select rows or columns with missing values (NA – Not Available)?
tableOfNAs = dfD.isna()
tableOfNAs
rowHasNaN = tableOfNAs.any(axis=1)
rowHasNaN
rowsWithNaN = dfD[rowHasNaN]
rowsWithNaN
columnHasNaN = tableOfNAs.any(axis=0)
columnHasNaN
columnsWithNaN = dfD[dfD.columns[columnHasNaN]]
columnsWithNaN

print(" "
      "")

#Replace known strings for missing values in data by NA-values
import numpy as np
missing_values = ['n/a', 'na', 'nan', 'N/A', 'NA', 'NaN', 'NAN', '--', 'Missing']
dfE = pd.read_csv('csv/errorFile4.csv', na_values = missing_values, sep=';', decimal='.')
print(dfE)

print(" "
      "")

#NA must be replaced by a well-known value
import numpy as np
result = ['Pass', 'Fail', np.nan, 'Fail', 'Fail']
id = np.arange(1,6)
dfF= pd.DataFrame({'ID':id, 'Result':result})
print(dfE)

print(" "
      "")

#problem: Replace NA with value
dfF.fillna(0, inplace=True)

print(" "
      "")

#The number of NA values is limited, but they hamper exploration.
lists = [[1, 'Pass', 15], [2, 'Fail', 4], [3, 'Pass', np.NaN], [4, 'Fail', 8], [5, 'Pass', np.NaN]]
dfG = pd.DataFrame(lists, columns = ['Id', 'Result', 'Score'])
print(dfG)

print(" "
      "")

#problem: Certain Python features can't handle NA values
dfG = dfG.dropna(axis='rows') # Remove the rows where there is at least 1 NA
#OR
dfG.dropna(axis='columns', inplace=True) # Remove the columns where there is at least 1 NA

print(" "
      "")

#Change a specific value into another value:
lists = [[1, 'Pass', 'm'], [2, 'Fail', 'm'], [3, 'Pass', np.NaN], [4, 'Fail', 'w'], [5, 'Pass', np.NaN]]
dfH = pd.DataFrame(lists, columns = ['Id', 'Result', 'Gender'])
dfH.loc[dfH.Gender == 'w', 'Gender'] = 'f'

print(" "
      "")

#(part of) the field content(s) must be removed or replaced (look for patterns  regular expression)
names= ['Jan', 'Annelies', 'Lisl', 'Lisa']
id = np.arange(1,5) # upper value not in created range
dfI = pd.DataFrame({'ID':id, 'Name':names})
dfI['Name'].replace(regex=['a'], value='A', inplace=True)

'''Test the following regular expressions:
'[aA]n' '^[aA]n' '[aA]n$' ' ^.is.$' '(n){2}'
'(e|l|L|i|s){3}' 'n? ' 'n+' 'i.*s'''

print(" "
      "")

#Python and dates:
'''
import datetime as dt
now = dt.datetime.now()
print(now)
date = dt.datetime(2018, 6, 1)
print(date.strftime('%Y - %B - %d'))
date_time_obj = dt.datetime.strptime(date_time_str, '%d/%m/%y %H:%M:%S')'''
#Strings instead of dates
dfJ = pd.read_csv("csv/errorFile5.csv", na_values = missing_values, sep=';', decimal='.')
dfJ.dtypes
dfJ.Date = pd.to_datetime(dfJ.Date)
dfJ.dtypes

print(dfJ)

print(" "
      "")

#Dates and/or times come from a different time zone
import pytz
import datetime as dt
timezone_newyork = pytz.timezone('America/New_York')
timezone_london = pytz.timezone('Europe/London')
date_time_obj = dt.datetime.strptime('18/09/19 01:55:19', '%d/%m/%y %H:%M:%S')
newyork_datetime_obj = date_time_obj.astimezone(timezone_newyork)
london_datetime_obj = date_time_obj.astimezone(timezone_london)
print(newyork_datetime_obj)
print(london_datetime_obj)
newyork_datetime_obj - london_datetime_obj
'''
>>> dfJ.Date.dt.tz_localize('Europe/Brussels’)
>>> dfJ.dtypes'''

print(" "
      "")

#Group rows
dfK = pd.read_csv('csv/dataset.csv', sep=';')
names = dfK.name.unique()

#problem: values from multiple rows into one row
pairs = list()
for name in names:
    pairs.append([name, (dfK.loc[dfK.name == name]['purchase'].sum())])
dfK_total = pd.DataFrame(pairs)
dfK_total.rename(columns = {dfK_total.columns[0] : 'Name', dfK_total.columns[1] : 'Total purchase'}, inplace = True)

print(" "
      "")

#Combining two datasets
dfL = pd.read_csv('csv/data1.csv', sep='; ')
dfM = pd.read_csv('csv/data2.csv', sep=';')

#problem: data is in different datasets and needs to be merged
df_merged = pd.merge(dfL, dfM, on='name')
df_merged = pd.merge(dfL, dfM, on='name', how='outer')