import pandas as pd
import math
import stat
import matplotlib.pyplot as plt


laptops = pd.read_csv('csv/laptops.csv', sep=';',decimal=',', header=0)

cpuGen = ['Sandy Bridge', 'Ivy Bridge', 'Haswell','Broadwell', 'Skylake', 'Kabylake']
cpuTypeLevels = ['i3', 'i5', 'i7']

laptops['cpuGeneration'] = laptops['cpuGeneration'].astype(pd.CategoricalDtype(categories=cpuGen, ordered=True))
laptops.cpuType = pd.Categorical(laptops['cpuType'], ordered=True, categories=cpuTypeLevels)

print(laptops)
print(laptops.info())

print(" "
      " ")

# Absolute frequencies
print(laptops.cpuType.value_counts())
print(laptops.cpuType.value_counts(dropna=False))
print(laptops.cpuType.value_counts().sort_index())

print(" "
      " ")

# Relative frequencies
print(laptops.brand.value_counts(normalize=True))
print((laptops.brand.value_counts(normalize=True)*100).round(1))

print(" "
      " ")

#Absolute cumulative frequencies:
print(laptops.cpuGeneration.value_counts().sort_index().cumsum())

print(" "
      " ")

#Cumulative percentages:
print((laptops.cpuGeneration.value_counts(normalize=True).sort_index().cumsum()*100).round(1))

print(" "
      " ")


# Exercise: Create a function that gets a vector as a
# parameter and puts all frequencies in a table.
def all_freq(x):
    t_abs = x.value_counts(dropna=False).sort_index()
    t_rel = (x.value_counts(dropna=False,normalize=True).sort_index()*100).round(1)
    t_abs_cum = x.value_counts(dropna=False).sort_index().cumsum()
    t_rel_cum = (x.value_counts(dropna=False,normalize=True).sort_index().cumsum()*100).round(1)
    return pd.DataFrame({'abs freq':t_abs,'rel freq':t_rel,'abs cum freq':t_abs_cum,'relcum freq':t_rel_cum})

all_freq(laptops.cpuGeneration)

print(" "
      " ")

# Univariate frequency distribution (= one-dimensional
# table):
# TODO: This following line does not work
#laptops$cpuType.x.value_counts(dropna=False).sort_index()

print(" "
      " ")

# Multivariate frequency distribution (=
# multidimensional table):

# Bivariate frequency distribution
pd.crosstab(laptops.brand, laptops.cpuType ,margins=True)
pd.crosstab( [laptops.brand, laptops.cpuType], laptops.RAM,margins=True)

print(" "
      " ")

# Classes
cutpoints=range(0, 1200, 100)
classes=pd.cut(laptops.diskspace, bins=cutpoints)
classes.value_counts().sort_index()

classes= pd.cut(laptops.diskspace , cutpoints, right=False)

# First, remove the NA values
diskspace = laptops.diskspace.dropna()

n=len(diskspace)
math.ceil(1+math.log2(n)) # Sturges
# TODO: This code does not work
# b=3.5*stat.stdev(diskspace)/(n**(1/3))
# math.ceil((diskspace.max(), diskspace.min())/b) # Scott
math.ceil(math.sqrt(n))

print(" "
      " ")

# Pie Chart
x = laptops.RAM.value_counts().sort_index()
l = ['1 GB', '2 GB', '4 GB', '8 GB', '16 GB']
plt.figure()
plt.pie(x, labels=l)
plt.title('RAM in laptops')
plt.show()

print(" "
      " ")

# Bar Chart
x = laptops.RAM.value_counts().sort_index()
l = ['1 GB', '2 GB', '4 GB', '8 GB', '16 GB']
plt.figure()
plt.bar(l, x)
plt.title('RAM in laptops')
plt.xlabel('RAM memory')
plt.ylabel('Number')
plt.show()

plt.figure()
ctC = pd.crosstab ( [laptops.brand , laptops.cpuType ], laptops.RAM , margins=False)
ctC.plot.bar(title='RAM in laptops', xlabel = 'Brand', stacked = True)
plt.show()

#Histogram
plt.figure()
laptops.diskspace.plot.hist(title ='RAM in laptops', xlabel = 'Disk space GB)GB)', ylabel = 'Number', stacked=True)
plt.show()

cutpoints=[ 0, 185, 375, 750, 1100]
plt.figure()
laptops.diskspace.plot. hist(bins=cutpoints, title ='RAM in laptops', xlabel = 'Disk space ( GB)', ylabel = 'Number', stacked = True)
plt.show()

# Frequency polygon
# TODO: Make your own function
# cutpoints = [0, 185, 375, 750, 1100]
# classes = pd.cut(laptops.diskspace , bins = cutpoints)
# x = classes.value_counts().sort_index().plot()
# plt.show

# Spider plots
x = laptops.brand
t = x.value_counts()
categories = t.index
values = t.values.tolist()
values += values[: 1] #add end point equal to start point
n = len(t)
m = max(values)
angles = [k/float(n) * 2 * math.pi for k in range(n)]
angles += angles[:1]

plt.figure()
ax = plt.subplot(polar=True)
plt.xticks( angles[:-1], categories, color ='grey',  size=8)
ax.set_rlabel_position(0)
plt. yticks ([k/4*m for k in range(4)],[ k/4*m for k in range(4)], color='grey', size=7)
plt.ylim(0, m)
plt.plot(angles, values, linewidth=1, linestyle='solid')
plt.fill(angles, values , 'b', alpha=0.1)
plt.show()
#TODO: Make your own function for this!

# Word Cloud
import wordcloud as wc
text = 'imagine there’s no heaven ...'
cloud = wc.WordCloud(max_font_size=70, max_words=40,background_color='white').generate(text)
plt.figure()
plt.imshow(cloud, interpolation='bilinear')
plt.axis('off')
plt.show()

# from PIL import Image
# def create_word_cloud(text,file_name):
# maskArray = np.array(Image.open(file_name))
# cloud = wc.WordCloud(background_color = "white",
# max_words = 200, mask = maskArray)
# cloud.generate(text)
# cloud.to_file("WC_"+file_name)
# plt.figure()
# plt.imshow(cloud, interpolation='bilinear')
# plt.axis('off')
# plt.show()
# create_word_cloud(text,'cloud.jpg')