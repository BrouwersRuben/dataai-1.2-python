import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

laptops = pd.read_csv("csv/laptops.csv", delimiter=";", decimal=",")
broadBand = pd.read_csv('csv/broadband.csv', delimiter=',', decimal=',')

broadBand['rounded sync speed'] = round(broadBand['avr sync speed'],0)
broadBand['rounded sync speed'] = broadBand['rounded sync speed'].astype(int)

def outlier_boundaries(x):
    Q1 = x.quantile(0.25)
    Q3 = x.quantile(0.75)
    I = Q3 - Q1
    return [ Q1 - 1.5 * I, Q3 + 1.5 * I ]

# Adjust the levels of the data to match the ordinal
# measurement scale
cpuGen = ["Sandy Bridge", "Ivy Bridge", "Haswell", "Broadwell", "Skylake", "Kabylake"]
laptops['cpuGeneration'] = laptops['cpuGeneration'].astype(pd.CategoricalDtype(categories=cpuGen, ordered=True))

print(laptops)

# mode
print(laptops.cpuGeneration.mode())

# median
# print(laptops.cpuGeneration.median())

# Make a function (usable for qualitative ordinal data):
def median_categorical(data):
    d = data.dropna()
    n = len(d)
    middle = np.floor(n/2)
    return d.sort_values().reset_index(drop=True)[middle]

print(median_categorical(laptops['RAM']))

# mean
print(laptops.diskspace.mean())

values = np.arange(0,7)
freq = [18, 20, 22, 10, 3, 1, 1]
print(sum(values*freq)/sum(freq))

# weighter mean
weight = [6, 11, 5, 3, 3, 7, 4, 7, 7, 3, 4]
score = [18, 15, 12, 10, 18, 13, 17, 15, 13, 12, 12]
# print(sum(weight*score)/sum(weight))

def calculateWeightedAverage(score, weight):
    """Example np arrays
    weight = [4,2,3,3]
    score = [20,21,25,26]"""
    return np.average(score, weights=weight)

print(calculateWeightedAverage(score, weight))

# Geometric mean
price = pd.Series([ 105 , 103 ,98])
print(np.exp(np.mean(np.log(price))))

# Harmonic mean
speed = pd.Series([ 120, 100 ])
print(1/np.mean(1/speed))

# Moving average
data = pd.Series([1,2,2,2,2,3,3,4,7,8,8,8,9,9,9 ])

def central(data):
    mode = data.mode()
    median = data.median()
    average = data.mean()
    geometric = np.exp(np.mean(np.log(data)))
    harmonic = 1/np.mean(1/data)
    return print("Mode: %8.2f, Median: %6.2f, Average: %5.2f, Geometric Average: %8.2f, Harmonic Average: %8.2f" % (mode, median, average, geometric, harmonic))

central(data)
central(broadBand['rounded sync speed'])

# Range
# max(x) - min(x)

# Quantiles
laptops.diskspace.quantile(q=[0.25, 0.5, 0.75])
laptops.diskspace.quantile(q=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6,0.7, 0.8, 0.9])

quartiles = laptops.diskspace.quantile(q=[0.25,0.5,0.75])
print(quartiles.iloc[2] - quartiles.iloc[0])
print(quartiles)

# BoxPlot
plt.figure()
data_to_plot = ['diskspace']
laptops.dropna().boxplot(data_to_plot)
plt.show()

# or
plt.figure()
data_to_plot = ['diskspace', 'RAM']
laptops.dropna().boxplot(data_to_plot)
plt.show()

# Mean deviation
print((laptops['diskspace']-laptops['diskspace'].mean()).mean())

# Absolute mean deviation
# abs(x - x.mean()).mean()

# Variance
# x.dropna().var()

# Standard Deviation
# x.dropna().std()

# Standard scores
from scipy import stats
Zlaptops = laptops[['diskspace', 'RAM']].dropna()
Zlaptops.diskspace = stats.zscore(Zlaptops.diskspace)
Zlaptops.RAM=stats.zscore(Zlaptops.RAM)

plt.figure()
data_to_plot = ['diskspace','RAM']
Zlaptops.boxplot(data_to_plot)
plt.show()