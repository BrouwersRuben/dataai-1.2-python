from durable.lang import *

with ruleset('risk'):
    # matching primitive array
    @when_all(m.payments.allItems((item > 100) & (item < 500)))
    def rule1(c):
        print('primitive array detected {0}'.format(c.m.payments))

    # matching object array
    @when_all(m.payments.allItems((item.amount < 250) | (item.amount >= 300)))
    def rule2(c):
        print('object array detected {0}'.format(c.m.payments))

    # pattern matching string array
    @when_all(m.cards.anyItem(item.matches('three.*'))) <--HERE
    def rule3(c):
        print('string array detected {0}'.format(c.m.cards))

    # matching nested arrays
    @when_all(m.payments.anyItem(item.allItems(item < 100)))
    def rule4(c):
        print('nested arrays detected {0}'.format(c.m.payments))
        
post('risk', {'payments': [ 150, 300, 450 ]})
post('risk', {'payments': [ { 'amount' : 200 }, { 'amount' : 300 }, { 'amount' : 450 } ]})
post('risk', {'cards': [ 'one card', 'two cards', 'three cards' ]}) <--HERE
post('risk', {'payments': [ [ 10, 20, 30 ], [ 30, 40, 50 ], [ 10, 20 ] ]}) 