from durable.lang import *
global board;
board = [1, 2, 3,
         4, 5, 6,
         7, 8, 9]

testjson = {'move': 'next', 'player': 'x'}
global jsonboard
jsonboard = {'player': 'x',
             'zero': '1',
             'one': '2',
             'two': '3',
             'three': '4',
             'four': '5',
             'five': '6',
             'six': '7',
             'seven': '8',
             'eigth': '9'}

def show():
    print(jsonboard.get('zero'),"|",jsonboard.get('one'),"|",jsonboard.get('two'))
    print("----------")
    print(jsonboard.get('three'),"|",jsonboard.get('four'),"|",jsonboard.get('five'))
    print("----------")
    print(jsonboard.get('six'),"|",jsonboard.get('seven'),"|",jsonboard.get('eigth'))

with ruleset('nextPosition'):
    # antecedent  # if board[4] != "o" and board[4] != "x": return 4
    @when_all(m.four != "o" and m.four != "x")
    def take4(c): # consequent
        jsonboard.__setitem__('four', c.m.player)
        show()
    # antecedent  # elif checkTwo("o", 0, 1) and board[2] != "o" and board[2] != "x": return 2
    @when_all(m.zero == "o" and m.one == "o" and m.two != "o" and m.two != "x")
    def take2(c): # consequent
        jsonboard.__setitem__('two', c.m.player)
        show()
    # antecedent  #elif checkTwo("o", 1, 2) and board[0] != "o" and board[0] != "x": return 0
    @when_all(m.one == "o" and m.two == "o" and m.zero != "o" and m.zero != "x")
    def take0(c): # consequent
        jsonboard.__setitem__('zero', c.m.player)
        show()
    # antecedent  #elif checkTwo("o", 0, 2) and board[1] != "o" and board[1] != "x": return 1
    @when_all(m.zero == "o" and m.two == "o" and m.one != "o" and m.one != "x")
    def take1(c): # consequent
        jsonboard.__setitem__('one', c.m.player)
        show()

#computer turn
#post('nextPosition', jsonboard)
#player turn
#jsonboard.__setitem__('two', 'o')
#show()
#computer turn
#post('nextPosition', jsonboard)
#post('nextPosition', {'move': 'next', 'player': 'o'})

#jsonboard.get('b1')
#jsonboard.__getitem__('0')
#jsonboard.__setitem__('0', 'test')
