import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sklearn.cluster as skl

hcExample = pd.read_csv ("csv/hclust.csv")
hcExample.index = range(1, hcExample.shape[0]+1)
hcmodel = skl.AgglomerativeClustering(n_clusters=4)
hcmodel.fit(hcExample)
# print the number of clusters found
print(hcmodel.n_clusters)
# print the labels for each point
print(hcmodel.labels_)

from scipy.cluster.hierarchy import linkage, dendrogram
colors = list(matplotlib.colors.cnames.keys()) # 148
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(5, 5))
distances = linkage(hcExample, method='single')
ax.set_title("Dendrogram")
ax.set_xlabel('point')
ax.set_ylabel('Euclidian distance')
ax.grid(linestyle='--', axis='y')
dgram = dendrogram(distances, labels=list(range(1, hcExample.shape[0]+1)), link_color_func=lambda x: colors[x], leaf_font_size=15., ax=ax)
plt.show()

# Cut a Tree into Groups of clusters
from scipy.cluster.hierarchy import cut_tree, linkage
hcExample = pd.read_csv("csv/hclust.csv")
hcExample.index = ['point {}'.format(i) for i in range(1,hcExample.shape[0]+1)]

distances = linkage(hcExample, method='single')
cuttree = cut_tree(distances, height=[2.5])
cuttree