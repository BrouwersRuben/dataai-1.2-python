import scipy.spatial.distance as dist
import pandas as pd
from sklearn.neighbors import DistanceMetric
import numpy as np
from scipy import stats
import matplotlib
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier

# Cluster Analysis
# Automated data exploration method, or unsupervised machine learning

# Supervised learning
# - Knowledge of the result
# - Learning with a human "expert"
# - data is "labeled"

# Unsupervised learning
# - No knowledge of the result
# - Self-guiding learning algorithm
# - Data is not "labaled"

# A cluster is a number of rows of a table that 
# "belong" or are "similar" to each other
# Row = a point in a n-dimensional space
# 2 rows are similar if the points are "close" to each other
# Distance between points must be small

# cluster analysis requires a distance function
# between points (or a similarity measure or
# disimilarity measure)

import scipy.spatial.distance as dist
example= pd.read_csv("csv/distance example.csv") # points are columns
example.index=example.subject.values
example = example.drop('subject',axis=1)
print ('Euclidean distance is', dist.euclidean(example.student1, example.student2))
print ('Manhattan distance is', dist.cityblock(example.student1, example.student2))

stanDev = [5,2,3,3,4]
exampleZ = pd.DataFrame()
exampleZ['student1'] = example.student1 / stanDev
exampleZ['student2'] = example.student2 / stanDev
exampleZ.index = example.index
print ('Standardized Euclidean distance is', dist.euclidean(exampleZ.student1, exampleZ.student2))

exampletrans = example.copy().transpose() # points are rows
from sklearn.neighbors import DistanceMetric
dist_obj = DistanceMetric.get_metric('euclidean')
dist_obj.pairwise(exampletrans)
dist_obj = DistanceMetric.get_metric('manhattan')
dist_obj.pairwise(exampletrans)

exampletrans = example.copy().transpose() # points are rows
from sklearn.neighbors import DistanceMetric
dist_obj = DistanceMetric.get_metric('euclidean')
dist_obj.pairwise(exampletrans)
dist_obj = DistanceMetric.get_metric('manhattan')
dist_obj.pairwise(exampletrans)

stanDev = [5,2,3,3,4] 
exampletransZ = exampletrans.copy()
for i in range(0,exampletransZ.shape[1]):
    exampletransZ.iloc[:,i] = exampletransZ.iloc[:,i] / stanDev[i]
dist_obj = DistanceMetric.get_metric('euclidean');
dist_obj.pairwise(exampletrans)

exampletrans = example.copy().transpose() # points are rows
from scipy.spatial.distance import cdist
cdist(exampletrans, exampletrans)
cdist(exampletrans, exampletrans, metric='cityblock')

stanDev = [5,2,3,3,4]
exampletransZ = exampletrans.copy()
for i in range(0,exampletransZ.shape[1]):
    exampletransZ.iloc[:,i] = exampletransZ.iloc[:,i] / stanDev[i]
cdist(exampletransZ, exampletransZ)

print(" \n");

# NOMINAL: to be avoided because you cannot define a distance
# or a similarity measure between nominal variables.

# ORDINAL: instead of a distance, use a similarity measure (e.g.
# rank correlation coefficient, median,...) that respects the
# ordinality of the data (possibly requiring a rank number to be
# associated with each value ranging from 1 to number of
# possible values).

# Note: Replacing qualitative values with numbers and then calculate
# distances (so-called cardinalization) is a wrong approach!

# Cluster analysis - K-means
from sklearn.cluster import KMeans
clusterExample = pd.read_csv("csv/clustering example.csv")
kmeansmodel = KMeans(n_clusters=4)
kmeansmodel.fit(clusterExample)
# print the number of iterations required to converge
print(kmeansmodel.n_iter_)
# print the final locations of the centroid
print(kmeansmodel.cluster_centers_)
# print the lowest SSE value
print(kmeansmodel.inertia_)
# print the cluster labels for each point
print(kmeansmodel.labels_)
# predict cluster label for a new point
kmeansmodel.predict([[1.25,0.09]])
# print the labels
print(kmeansmodel.labels_)
# plot the points and the cluster
fig, ax = plt.subplots(figsize=(5, 5))
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.grid(linestyle='--')
ax.set_title('K-Means')
# put unique labels in labels and retrieve the list of markers
labels = set(kmeansmodel.labels_)
markers = list(matplotlib.markers.MarkerStyle.markers.keys())
# plot all points in a different color and marker
for m, l in zip(markers, labels):
    indices = np.where(kmeansmodel.labels_ == l)[0] # is a tuple => [0]
    ax.scatter(clusterExample.loc[indices, 'x'],
               clusterExample.loc[indices, 'y'], marker=m, s=50)
# plot the cluster center in the middle of the clusterplot
ax.scatter(kmeansmodel.cluster_centers_[:, 0], kmeansmodel.cluster_centers_[:, 1], marker= "o" , c='black', s=100)
plt.show()

# Hierarchical clusters
hcExample = pd.read_csv ("csv/hclust.csv")
hcExample.index = range(1, hcExample.shape[0]+1)
hcmodel = AgglomerativeClustering(n_clusters=4)
hcmodel.fit(hcExample)
# print the number of clusters found
print(hcmodel.n_clusters)
# print the labels for each point
print(hcmodel.labels_)
