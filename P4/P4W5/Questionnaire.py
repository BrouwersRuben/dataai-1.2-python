from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

studenq = pd.read_csv('csv/Questionnaire 20-21.csv', delimiter=';', decimal=',')

studenq = studenq.dropna(axis='rows').copy()
biometric = studenq[['Shoe Size', 'Length']].copy()
biometric.info()
# check the preconditions (here: correlations)
biometric.corr()
# standardize the data
biometricZ = StandardScaler().fit_transform(biometric)
# Perform the PCA
pca_dim = min(biometricZ.shape[1], biometricZ.shape[0])
pcamodel = PCA(n_components= pca_dim)
principalComponents = pcamodel.fit_transform(biometricZ)
# results PCA
print('Explained variance in %:', pcamodel.explained_variance_ratio_)
# Replace the two colums by the new one (= PC1)
studenq.pop('Shoe Size')
studenq.pop('Length')
studenq['biometic'] = principalComponents[:,0]

# studenq_raw.info() # 1° and last columns (‘ID’ and ‘class’) have to be removed.
# studenq = studenq_raw[["Shoe Size", "Length"]].copy();

# studenq = studenq.dropna(axis='rows')

# studenq.corr()

# studenqZ = StandardScaler().fit_transform(studenq)
# #Better:
# studenqZ =pd.DataFrame()
# for column in studenq:
#     mean = studenq[column].mean()
#     standev = studenq[column].std()
#     studenqZ[column] = (studenq[column] - mean)/standev
# studenqZ.index= studenq.index.values

# pca_dim = min(studenqZ.shape[1], studenqZ.shape[0])
# pcamodel = PCA(pca_dim)
# principalComponents = pcamodel.fit_transform(studenqZ)

# print('Explained variance in %:', pcamodel.explained_variance_ratio_)

# principalDf= pd.DataFrame()
# principalDf['Shoe Size'] = principalComponents[:,0]
# principalDf['Length'] = principalComponents[:,1]
# fig, ax = plt.subplots(figsize=(5, 5))
# ax.set_xlabel('Shoe Size')
# ax.set_ylabel('Length')
# labels_plot = set(principalDf['Shoe Size'])
# markers = list(matplotlib.markers.MarkerStyle.markers.keys())
# for m, l in zip(markers, labels_plot):
#     indices = np.where(principalDf['Shoe Size'] == l)[0]
#     ax.scatter(principalDf.loc[indices, 'Shoe Size'], principalDf.loc[indices, 'Length'], marker=m, s=50)
# ax.legend(labels_plot)
# plt.show()