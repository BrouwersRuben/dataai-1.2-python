# Seasonal trend forecasting
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt

revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]
# With the previous technique, we first try to determine the general trend and set a pattern for it.
# But you can also directly search for a trend in every point of the pattern. For example, if the
# season size equals 4, we can look for points 0, 4, 8, 12, ... These points correspond with all first
# points of the season. We can look for a trend in those points. Then we look for points 1, 5, 9, 13,
# ... We can also find a trend in these points. We continue this way until we have a trend line for
# each point of the season. The trend line can be determined with regression.

def general_regression(x, y, degree=1, exp=False):
    func=lambda x:x # def fun(x): return[x]
    inv_func=lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x:func(line(x))
    yyy = pd.Series(predict(x))
    se = math.sqrt(((yyy-y)**2).mean())
    R2 = (x.corr(inv_func(y)))**2
    result = [se, R2, predict]
    index = ['se', 'R2', 'predict']
    for i in range(1,len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

n = len(revenues)
period = 4
for i in range(0,period):
    x = []
    y = []
    for k in range(0, int(math.floor(n-1-i)/period)+1):
        index = i + k * period
        x = x + [ index ]
        y = y + [ revenues[index] ]
        print('x= ', x)
        print('y= ', y)

# Now we have to find the trend line for each of those lines. We do this with regression. We do m
# times a regression and put all these models in a list:

def findRegressionLines(past, period, degree=1, exp=False):
    n = len(past)
    regressionlines= []
    for i in range(0,period):
        x = []
        y = []
        for k in range(0, int(math.floor(n-1-i)/period)+1):
            index = i + k * period
            x = x + [ index ]
            y = y + [ revenues[index] ]
            reg = general_regression(pd.Series(x), pd.Series(y), degree, exp=exp)
        regressionlines = regressionlines + [ reg ]
    return regressionlines

def seasonalTrendForecast(past, period, degree=1, exp=False):
    regressionlines = findRegressionLines(past, period, degree, exp)
    def predict(x):
        predicted = []
        for i in range(0, len(x)):
            y = regressionlines[i%period].predict(x[i])
            predicted = predicted + [y]
        return predicted
    return predict

# TODO:
# In this example we will take a quadratic regression (it gives good results here, see for yourself the
# alternatives):
f = seasonalTrendForecast(revenues, 4, degree=2)

predicted = f(range(20, 24))
print(predicted)
