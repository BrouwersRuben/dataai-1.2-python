# Making predictions
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt

revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]


# If we want to make predictions, we will continue the chart of the trend and repeat the pattern. Of
# course we cannot predict the noise, so we leave it out.
# To be able to continue the trend, we can perform a regression so we have a formula for T(i). A
# linear regression, for example, yields the following formula:

def general_regression(x, y, degree=1, exp=False):
    func=lambda x:x # def fun(x): return[x]
    inv_func=lambda x:x
    if (exp):
        func=np.exp
        inv_func=np.log
    model = np.polyfit(x, inv_func(y), degree)
    line = np.poly1d(model)
    predict = lambda x:func(line(x))
    yyy = pd.Series(predict(x))
    se = math.sqrt(((yyy-y)**2).mean())
    R2 = (x.corr(inv_func(y)))**2
    result = [se, R2, predict]
    index = ['se', 'R2', 'predict']
    for i in range(1,len(model)+1):
        result = np.append(result, model[-i])
        index += chr(i+96) # to obtain the characters a,b,...
    result = pd.Series(result)
    result.index = index
    return result

def trendForecastingModel(past):
    n = len(past)
    x = pd.Series(range(0,n))
    y = pd.Series(past)
    reg = general_regression(x, y)
    return reg

myTrend = trendForecastingModel(revenues)
print(myTrend(20))

reg = general_regression(pd.Series(range(2,18)),pd.Series(trend[2:18]), 3)
print(reg)

predicted = reg.predict(range(20, 24)) + season[0:4]
print(predicted)