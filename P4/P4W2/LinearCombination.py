# Linear combination
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt
revenues = [20, 100, 175, 13, 37, 136, 245, 26, 75, 155, 326, 48, 92, 202, 384, 82, 176, 282, 445, 181]
predictedTest = [287.9464, 393.2149, 543.8526, 318.7714]

def calculateWeights(period, past):
    n = len(past)
    if (n<2*period):
        return math.nan
    v = past[(n-2*period):(n-period)]
    for i in range(2,period+1):
        v = v + past[(n-2*period+i-1):(n-period+i-1)]
    M = np.array(v).reshape(period, period)
    v = past[(n-period):n]
    return np.linalg.solve(M, v)

a = calculateWeights(4, revenues)
print(a)

period = 4
n = len(revenues)
prediction = (revenues[(n-period):n]*a).sum()
print(prediction)

def linearCombinationForecasting(period):
    def result(past):
        n = len(past)
        if (n<2*period):
            return math.nan
        a = calculateWeights(period, past)
        return (past[(n-period):n]*a).sum()
    return result

forecast = linearCombinationForecasting(4)
print(forecast(revenues))

past = revenues
predicted = [ ]
for i in range(0,4):
    forecast = linearCombinationForecasting(4)
    next = forecast(past)
    predicted = predicted + [next]
    past = past + [next]

for i in predicted:
    print(i)


plt.figure()
plt.plot(range(0,20), revenues, 'o-')
plt.plot(range(21, 25), predicted, 'or-')
plt.xlabel('quarter')
plt.ylabel('revenue (EUR)')
plt.title('Revenues the last 5 years')
plt.show()